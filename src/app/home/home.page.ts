import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  energy = 0;
  cards = 0;

  axies = [];

  constructor() {
    this.reset();
  }

  nextTurn(){
    this.energy += 2;
    this.cards += 3;
  }

  useCard(index, part) {
    if(this.axies[index][part].count > 0) {
      this.axies[index][part].count -= 1;
    }
    if( this.cards > 0) {
      this.cards -= 1;
    }
    if(this.energy > 0) {
      this.energy -= this.axies[index][part].cost
    }
  }

  addCard(value){
    this.cards += value;
  }

  addEnergy(value) {
    this.energy += value;
  }

  reset() {
    this.energy = 3;
    this.cards = 6;
    this.axies =  [
      //front
      {
        back: {count:2, cost: 1},
        horn: {count:2, cost: 1},
        mouth: {count:2, cost: 1},
        tail : {count:2, cost: 1}
      },
      // mid
      {
        back: {count:2, cost: 1},
        horn: {count:2, cost: 1},
        mouth: {count:2, cost: 1},
        tail : {count:2, cost: 1}
      }, 
      // back
      {
        back: {count:2, cost: 1},
        horn: {count:2, cost: 1},
        mouth: {count:2, cost: 1},
        tail : {count:2, cost: 1}
      }
    ]
  }

  changeCost(index, part, event) {
    console.log("event", event);
    
    this.axies[index][part].cost = event.detail.value;

  }

}
